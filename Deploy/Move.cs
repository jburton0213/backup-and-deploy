﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Deploy
{
    class Move
    {
        public String source { get; set; }
        public String destination { get; set; }
        public String movetime { get; set; }
        public String filename { get; set; }
        public String movetype { get; set; }

        public static void New(FileInfo sourceFile, DirectoryInfo destinationDirectory, string moveType, string directoryStructure)
        {
            //TODO: Need to put this in app.config or something. 

            var moveLogDirectory = "MoveLogs";
            if (!(Directory.Exists(moveLogDirectory)))
            {
                Directory.CreateDirectory(moveLogDirectory);
            }

            if (!(Directory.Exists(moveLogDirectory + "\\" + directoryStructure))) 
            {
                Directory.CreateDirectory(moveLogDirectory + "\\" + directoryStructure);
            }

            var todaysMoveFile = moveLogDirectory + "\\" + directoryStructure + "\\" + DateTime.Today.Year + "-" + DateTime.Today.Month + "-" + DateTime.Today.Day + ".json";

            if (!File.Exists(todaysMoveFile))
            {
                var file = File.Create(todaysMoveFile);
                file.Close();
            }

            var movesJSONString = File.ReadAllText(todaysMoveFile);

            Moves moves = new Moves();

            if (!(movesJSONString == ""))
            {
                moves = JsonConvert.DeserializeObject<Moves>(movesJSONString);
            }

            Move m = new Move();

            m.source = sourceFile.FullName;
            m.destination = destinationDirectory.FullName;
            m.filename = sourceFile.Name;
            m.movetype = moveType;
            m.movetime = DateTime.Now.ToString();

            if (moves.data == null)
            {
                moves.data = new List<Move>();
            }

            moves.data.Add(m);

            var newJSONString = JsonConvert.SerializeObject(moves);

            File.WriteAllText(todaysMoveFile, newJSONString);
        }
    }

    class Moves
    {
        public List<Move> data { get; set; }
    }
}
