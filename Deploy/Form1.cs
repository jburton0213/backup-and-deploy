﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Web;
using Newtonsoft.Json;
using System.IO;
using System.Security.Cryptography;
using ListViewSorter;

namespace Deploy
{
    public partial class Form1 : Form
    {
        public DirectoryInfo StartingDirectory { get; set; }
        public bool connected { get; set; }
        private ListViewColumnSorter lvwColumnSorter;

        public Form1()
        {
            StartingDirectory = new DirectoryInfo("C:\\");
            InitializeComponent();
            InitializeApplication();
            lvwColumnSorter = new ListViewColumnSorter();
            this.moveListView.ListViewItemSorter = lvwColumnSorter;
        }

        private void InitializeApplication()
        {
            //TODO:Need to put my JSONs in a logical place.
            var movesTextString = System.IO.File.ReadAllText("Moves.json");
            var sAndDsTextString = System.IO.File.ReadAllText("SourcesAndDestinations.json");
            
            SourcesAndDestinations sads = JsonConvert.DeserializeObject<SourcesAndDestinations>(sAndDsTextString);

            sads.data.Add(new SourceAndDestination { destination = "", source = "", name = "New" });

            sAndDDropdown.ValueMember = null;
            sAndDDropdown.DisplayMember = "name";
            sAndDDropdown.DataSource = sads.data;
        }

        private void sAndDDropdown_SelectedIndexChanged(object sender, EventArgs e)
        {
            dynamic obj = sAndDDropdown.SelectedItem;

            if (StartingDirectory.FullName == obj.source & connected)
            {
                connectButton.Text = "Disconnect";
                this.connectButton.Click += new System.EventHandler(this.disconnectButton_Click);
                this.connectButton.Click -= this.connectButton_Click; 
            }
            else if (connected)
            {
                connectButton.Text = "Connect";
                this.connectButton.Click +=  new System.EventHandler(this.connectButton_Click);
                this.connectButton.Click -= this.disconnectButton_Click;
            }
        }

        private void connectButton_Click(object sender, EventArgs e)
        {
            folderUpButton.Enabled = false;
            connected = true;

            sourceListView.Items.Clear();
            destinationListView.Items.Clear();

            dynamic obj = sAndDDropdown.SelectedItem;

            sourceLabel.Text = obj.source;
            destinationLabel.Text = obj.destination;

            DirectoryInfo sinfo = new DirectoryInfo(obj.source);
            DirectoryInfo dinfo = new DirectoryInfo(obj.destination);

            StartingDirectory = sinfo;

            fillListViewWithDirectories(sinfo, sourceListView);
            fillListViewWithDirectories(dinfo, destinationListView);
            fillListViewWithFiles(sinfo, sourceListView);
            fillListViewWithFiles(dinfo, destinationListView);

            sAndDDropdown.Enabled = false;

            this.connectButton.Click += new System.EventHandler(this.disconnectButton_Click);
            this.connectButton.Click -= this.connectButton_Click;
            connectButton.Text = "Disconnect";

            populateMoveListView(sAndDDropdown.Text);
            lvwColumnSorter.SortColumn = 1;
            lvwColumnSorter.Order = SortOrder.Descending;
            this.moveListView.Sort();
        }

        private void populateMoveListView(string configuration)
        {
            var backupDirectory = "MoveLogs\\" + configuration;
            
            if (Directory.Exists(backupDirectory))
            {
                var jsonList = Directory.GetFiles(backupDirectory);
                foreach (string x in jsonList)
                {
                    var xstring = File.ReadAllText(x);

                    Moves moves = JsonConvert.DeserializeObject<Moves>(xstring);

                    ListViewItem.ListViewSubItem[] subItems;
                    ListViewItem item = null;

                    foreach (Move m in moves.data)
                    {
                        item = new ListViewItem(m.filename);
                        subItems = new ListViewItem.ListViewSubItem[]
                        {
                        new ListViewItem.ListViewSubItem(item, m.movetime),
                        new ListViewItem.ListViewSubItem(item, m.source)
                        };

                        item.SubItems.AddRange(subItems);
                        moveListView.Items.Add(item);
                    }
                }
            }

        }

        public void fillListViewWithFiles(DirectoryInfo dinfo, ListView lview)
        {
            ListViewItem.ListViewSubItem[] subItems;
            ListViewItem item = null;

            foreach (FileInfo file in dinfo.GetFiles())
            {
                item = new ListViewItem(file.Name, 1);
                subItems = new ListViewItem.ListViewSubItem[] { new ListViewItem.ListViewSubItem(item, file.LastAccessTime.ToString()) };
                item.SubItems.AddRange(subItems);
                lview.Items.Add(item);
            }
        }

        public void fillListViewWithDirectories(DirectoryInfo dinfo, ListView lview)
        {
            ListViewItem.ListViewSubItem[] subItems;
            ListViewItem item = null;

            foreach (DirectoryInfo dir in dinfo.GetDirectories())
            {
                item = new ListViewItem(dir.Name, 0);
                subItems = new ListViewItem.ListViewSubItem[] { new ListViewItem.ListViewSubItem(item, dir.LastAccessTime.ToString()) };
                item.SubItems.AddRange(subItems);
                lview.Items.Add(item);
            }
        }

        private void sourceListView_DoubleClick(object sender, EventArgs e)
        {
            String clickedSourceDirectory = sourceLabel.Text + "\\" + sourceListView.SelectedItems[0].Text;
            String clickedDestinationDirectory = destinationLabel.Text + "\\" + sourceListView.SelectedItems[0].Text;
            FileAttributes attr = File.GetAttributes(clickedSourceDirectory);

            if (attr.HasFlag(FileAttributes.Directory))
            {
                switchDirectory(clickedSourceDirectory, clickedDestinationDirectory);
            }

        }

        private void switchDirectory(string spath, string dpath)
        {
            sourceListView.Items.Clear();
            destinationListView.Items.Clear();

            DirectoryInfo spathDInfo = new DirectoryInfo(spath);
            DirectoryInfo dpathDinfo = new DirectoryInfo(dpath);

            fillListViewWithDirectories(spathDInfo, sourceListView);
            fillListViewWithFiles(spathDInfo, sourceListView);

            fillListViewWithDirectories(dpathDinfo, destinationListView);
            fillListViewWithFiles(dpathDinfo, destinationListView);

            sourceLabel.Text = spathDInfo.FullName;
            destinationLabel.Text = dpathDinfo.FullName;

            if (spathDInfo.FullName == StartingDirectory.FullName)
            {
                folderUpButton.Enabled = false;
            }
            else
            {
                folderUpButton.Enabled = true;
            }
        }

        private void folderUpButton_Click(object sender, EventArgs e)
        {
            DirectoryInfo spathInfo = new DirectoryInfo(sourceLabel.Text);
            DirectoryInfo dpathInfo = new DirectoryInfo(destinationLabel.Text);

            switchDirectory(spathInfo.Parent.FullName, dpathInfo.Parent.FullName);
        }

        private void disconnectButton_Click(object sender, EventArgs e)
        {
            sourceListView.Items.Clear();
            destinationListView.Items.Clear();
            connected = false;
            sAndDDropdown.Enabled = true;
            sourceLabel.Text = "";
            destinationLabel.Text = "";

            this.connectButton.Click += new System.EventHandler(this.connectButton_Click);
            this.connectButton.Click -= this.disconnectButton_Click;
            connectButton.Text = "Connect";
            moveListView.Items.Clear();
        }

        private void LeftToRightButton_Click(object sender, EventArgs e)
        {
            if (sourceListView.SelectedIndices.Count > 0 )
            {
                var sourceFilesToMove = sourceListView.SelectedItems;

                foreach (ListViewItem item in sourceFilesToMove)
                {
                    String sourceFile = sourceLabel.Text + "\\" + item.Text;
                    String destinationDirectory = destinationLabel.Text;

                    BackupAndCopyFile(sourceFile, destinationDirectory);
                    AddToMoveListView(sourceFile, destinationDirectory);
                }

                
            }
            else
            {
                MessageBox.Show("Select a file, brah!");
            }
        }

        private void AddToMoveListView(string sourceFile, string destinationDirectory)
        {
            ListViewItem.ListViewSubItem[] subItems;
            ListViewItem item = null;
            FileInfo fi = new FileInfo(sourceFile);

            item = new ListViewItem(fi.Name);
            subItems = new ListViewItem.ListViewSubItem[]
            {
                new ListViewItem.ListViewSubItem(item, DateTime.Now.ToString()),
                new ListViewItem.ListViewSubItem(item, destinationDirectory)
            };

            item.SubItems.AddRange(subItems);
            moveListView.Items.Insert(0, item);
        }

        private void RightToLeftButton_Click(object sender, EventArgs e)
        {
            if (destinationListView.SelectedIndices.Count > 0)
            {
                var sourceFilesToMove = destinationListView.SelectedItems;

                foreach (ListViewItem item in sourceFilesToMove)
                {
                    String sourceFile = destinationLabel.Text + "\\" + item.Text;
                    String destinationDirectory = sourceLabel.Text;

                    //TODO: Need to seriously look at this one again, because this ante gonna work.  I've specialized the BackupAndCopyFile too much\
                    //for the left to right button.  
                    BackupAndCopyFile(sourceFile, destinationDirectory);
                }
            }
            else
            {
                MessageBox.Show("Select a file, brah!");
            }
        }

        public bool CheckIfFileIsBackedUp(string backupfile, string originalFile)
        {
            try
            {
                FileInfo backedupFI = new FileInfo(backupfile);
                FileInfo originalFI = new FileInfo(originalFile);

                String backedupMD5String = generateMD5String(backedupFI);
                String originalMD5String = generateMD5String(originalFI);
                
                if (backedupMD5String == originalMD5String)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        private string BackupFile(string v)
        {
            FileInfo fileToBackup = new FileInfo(v);

            var backupDirectory = createBackupDirectories();

            try
            {
                String backupTarget = backupDirectory + "\\" + fileToBackup.Name;

                if (!File.Exists(backupTarget))
                {
                    File.Copy(fileToBackup.FullName, backupTarget);
                    return backupTarget;
                }
                else
                {
                    FileInfo fi = new FileInfo(backupTarget);
                    string fn = Path.GetFileNameWithoutExtension(fi.FullName);
                    int counter = 1;
                    string nFName = fi.DirectoryName + "\\" + fn + "_" + counter + fi.Extension;
                    while (File.Exists(nFName))
                    {
                        counter++;
                        nFName = fi.DirectoryName + "\\" + fn + "_" + counter + fi.Extension;
                    }

                    File.Copy(fileToBackup.FullName, nFName);
                    return nFName;

                }

            }
            catch(Exception e)
            {
                return "";
            }
        }

        private string createBackupDirectories()
        {
            //This method creates the backup directories if they don't exist,
            //and then returns the backup directory path for the file being backed up.

            DateTime today = DateTime.Today;
            //TODO: Need to allow the user to specify where they want their backup directory to be. 
            String backuproot = "Backups";

            if (!Directory.Exists(backuproot))
            {
                Directory.CreateDirectory(backuproot);
            }

            String todaysPath = backuproot + "\\" + today.Year + "-" + today.Month + "-" + today.Day;

            if (!Directory.Exists(todaysPath))
            {
                Directory.CreateDirectory(todaysPath);
            }

            dynamic obj = sAndDDropdown.SelectedItem;
            String backupPathforProject = todaysPath + "\\" + obj.name;

            if (!Directory.Exists(backupPathforProject))
            {
                Directory.CreateDirectory(backupPathforProject);
            }

            return backupPathforProject;
        }

        public void BackupAndCopyFile(String sourcefileIn, String destinationPathIn)
        {
            FileInfo sourceFile = new FileInfo(sourcefileIn);
            DirectoryInfo destinationDirectory = new DirectoryInfo(destinationPathIn);
            String destinationFile = destinationDirectory.FullName + "\\" + sourceFile.Name;

            if (File.Exists(destinationFile))
            {
                //TODO: Copy file to backup, then check to make sure file copied.  If file copied correctly and file in backup
                //directory is exact copy of the file in the destination directory, then copy and overwrite file in destination directory. 
                var backupfile = BackupFile(destinationFile);
                var backupstatus = CheckIfFileIsBackedUp(backupfile, destinationFile);

                if (backupstatus)
                {
                    File.Copy(sourceFile.FullName, destinationFile, true);
                    Deploy.Move.New(sourceFile, new DirectoryInfo(destinationFile), "OVERWRITE", sAndDDropdown.Text);
                }
                else
                {
                    //TODO: Need to put some logging here and be a bit more descriptive.
                    MessageBox.Show("The MD5 hash's of the backupfile and destination file didn't match!" + Environment.NewLine);
                }
            }
            else
            {
                FileAttributes attr = File.GetAttributes(sourceFile.FullName);

                if (!(attr.HasFlag(FileAttributes.Directory)))
                {
                    File.Copy(sourceFile.FullName, destinationDirectory.FullName);
                    Deploy.Move.New(sourceFile, destinationDirectory, "NEW", sAndDDropdown.Text);
                }
                else
                {
                    MessageBox.Show("You can't move a Directory Brah!");
                }
                
            }
        }

        public string generateMD5String (FileInfo filename)
        {
            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(filename.FullName))
                {
                    return Encoding.Default.GetString(md5.ComputeHash(stream));
                }
            }
        }

        private void moveListView_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            // Determine if clicked column is already the column that is being sorted.
            if (e.Column == lvwColumnSorter.SortColumn)
            {
                // Reverse the current sort direction for this column.
                if (lvwColumnSorter.Order == SortOrder.Ascending)
                {
                    lvwColumnSorter.Order = SortOrder.Descending;
                }
                else
                {
                    lvwColumnSorter.Order = SortOrder.Ascending;
                }
            }
            else
            {
                // Set the column number that is to be sorted; default to ascending.
                lvwColumnSorter.SortColumn = e.Column;
                lvwColumnSorter.Order = SortOrder.Ascending;
            }

            // Perform the sort with these new sort options.
            this.moveListView.Sort();
        }
    }
}
