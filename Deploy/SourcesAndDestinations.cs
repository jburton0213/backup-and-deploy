﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Deploy
{
    class SourceAndDestination
    {
        public String destination { get; set; }
        public String name { get; set; }
        public String source { get; set; }
    }   

    class SourcesAndDestinations
    {
        public List<SourceAndDestination> data { get; set; }
    }
}
