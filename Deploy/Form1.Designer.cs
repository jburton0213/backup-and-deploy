﻿namespace Deploy
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.sourceListView = new System.Windows.Forms.ListView();
            this.sFileName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.sModified = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.destinationListView = new System.Windows.Forms.ListView();
            this.dName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.dModified = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel2 = new System.Windows.Forms.Panel();
            this.folderUpButton = new System.Windows.Forms.Button();
            this.destinationLabel = new System.Windows.Forms.Label();
            this.sourceLabel = new System.Windows.Forms.Label();
            this.connectButton = new System.Windows.Forms.Button();
            this.sAndDDropdown = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.RightToLeftButton = new System.Windows.Forms.Button();
            this.LeftToRightButton = new System.Windows.Forms.Button();
            this.moveListView = new System.Windows.Forms.ListView();
            this.fName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.moved = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.DestinationDirectory = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // sourceListView
            // 
            this.sourceListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.sFileName,
            this.sModified});
            this.sourceListView.Location = new System.Drawing.Point(532, 0);
            this.sourceListView.Name = "sourceListView";
            this.sourceListView.Size = new System.Drawing.Size(318, 507);
            this.sourceListView.SmallImageList = this.imageList1;
            this.sourceListView.TabIndex = 0;
            this.sourceListView.UseCompatibleStateImageBehavior = false;
            this.sourceListView.View = System.Windows.Forms.View.Details;
            this.sourceListView.DoubleClick += new System.EventHandler(this.sourceListView_DoubleClick);
            // 
            // sFileName
            // 
            this.sFileName.Text = "Name";
            this.sFileName.Width = 135;
            // 
            // sModified
            // 
            this.sModified.Text = "Last Modified";
            this.sModified.Width = 179;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "folder.jpg");
            this.imageList1.Images.SetKeyName(1, "document.png");
            // 
            // destinationListView
            // 
            this.destinationListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.dName,
            this.dModified});
            this.destinationListView.Location = new System.Drawing.Point(903, 0);
            this.destinationListView.Name = "destinationListView";
            this.destinationListView.Size = new System.Drawing.Size(318, 507);
            this.destinationListView.SmallImageList = this.imageList1;
            this.destinationListView.TabIndex = 0;
            this.destinationListView.UseCompatibleStateImageBehavior = false;
            this.destinationListView.View = System.Windows.Forms.View.Details;
            // 
            // dName
            // 
            this.dName.Text = "Name";
            this.dName.Width = 144;
            // 
            // dModified
            // 
            this.dModified.Text = "Last Modifed";
            this.dModified.Width = 170;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.folderUpButton);
            this.panel2.Controls.Add(this.destinationLabel);
            this.panel2.Controls.Add(this.sourceLabel);
            this.panel2.Controls.Add(this.connectButton);
            this.panel2.Controls.Add(this.sAndDDropdown);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1229, 64);
            this.panel2.TabIndex = 1;
            // 
            // folderUpButton
            // 
            this.folderUpButton.Enabled = false;
            this.folderUpButton.Image = global::Deploy.Properties.Resources.up_folder_icon_544801;
            this.folderUpButton.Location = new System.Drawing.Point(743, 3);
            this.folderUpButton.Name = "folderUpButton";
            this.folderUpButton.Size = new System.Drawing.Size(36, 23);
            this.folderUpButton.TabIndex = 4;
            this.folderUpButton.UseVisualStyleBackColor = true;
            this.folderUpButton.Click += new System.EventHandler(this.folderUpButton_Click);
            // 
            // destinationLabel
            // 
            this.destinationLabel.AutoSize = true;
            this.destinationLabel.Location = new System.Drawing.Point(695, 30);
            this.destinationLabel.MaximumSize = new System.Drawing.Size(318, 0);
            this.destinationLabel.Name = "destinationLabel";
            this.destinationLabel.Size = new System.Drawing.Size(0, 13);
            this.destinationLabel.TabIndex = 3;
            // 
            // sourceLabel
            // 
            this.sourceLabel.AutoSize = true;
            this.sourceLabel.Location = new System.Drawing.Point(324, 30);
            this.sourceLabel.MaximumSize = new System.Drawing.Size(318, 0);
            this.sourceLabel.Name = "sourceLabel";
            this.sourceLabel.Size = new System.Drawing.Size(0, 13);
            this.sourceLabel.TabIndex = 2;
            // 
            // connectButton
            // 
            this.connectButton.Location = new System.Drawing.Point(662, 3);
            this.connectButton.Name = "connectButton";
            this.connectButton.Size = new System.Drawing.Size(75, 23);
            this.connectButton.TabIndex = 1;
            this.connectButton.Text = "Connect";
            this.connectButton.UseVisualStyleBackColor = true;
            this.connectButton.Click += new System.EventHandler(this.connectButton_Click);
            // 
            // sAndDDropdown
            // 
            this.sAndDDropdown.FormattingEnabled = true;
            this.sAndDDropdown.Location = new System.Drawing.Point(326, 6);
            this.sAndDDropdown.Name = "sAndDDropdown";
            this.sAndDDropdown.Size = new System.Drawing.Size(318, 21);
            this.sAndDDropdown.TabIndex = 0;
            this.sAndDDropdown.SelectedIndexChanged += new System.EventHandler(this.sAndDDropdown_SelectedIndexChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.RightToLeftButton);
            this.panel1.Controls.Add(this.LeftToRightButton);
            this.panel1.Controls.Add(this.moveListView);
            this.panel1.Controls.Add(this.destinationListView);
            this.panel1.Controls.Add(this.sourceListView);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 71);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1229, 513);
            this.panel1.TabIndex = 2;
            // 
            // RightToLeftButton
            // 
            this.RightToLeftButton.Location = new System.Drawing.Point(856, 223);
            this.RightToLeftButton.Name = "RightToLeftButton";
            this.RightToLeftButton.Size = new System.Drawing.Size(41, 23);
            this.RightToLeftButton.TabIndex = 3;
            this.RightToLeftButton.Text = "<";
            this.RightToLeftButton.UseVisualStyleBackColor = true;
            this.RightToLeftButton.Click += new System.EventHandler(this.RightToLeftButton_Click);
            // 
            // LeftToRightButton
            // 
            this.LeftToRightButton.Location = new System.Drawing.Point(856, 194);
            this.LeftToRightButton.Name = "LeftToRightButton";
            this.LeftToRightButton.Size = new System.Drawing.Size(41, 23);
            this.LeftToRightButton.TabIndex = 2;
            this.LeftToRightButton.Text = ">";
            this.LeftToRightButton.UseVisualStyleBackColor = true;
            this.LeftToRightButton.Click += new System.EventHandler(this.LeftToRightButton_Click);
            // 
            // moveListView
            // 
            this.moveListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.fName,
            this.moved,
            this.DestinationDirectory});
            this.moveListView.Location = new System.Drawing.Point(3, 3);
            this.moveListView.Name = "moveListView";
            this.moveListView.Size = new System.Drawing.Size(485, 507);
            this.moveListView.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.moveListView.TabIndex = 1;
            this.moveListView.UseCompatibleStateImageBehavior = false;
            this.moveListView.View = System.Windows.Forms.View.Details;
            this.moveListView.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.moveListView_ColumnClick);
            // 
            // fName
            // 
            this.fName.Text = "File Name";
            this.fName.Width = 148;
            // 
            // moved
            // 
            this.moved.Text = "Moved";
            this.moved.Width = 181;
            // 
            // DestinationDirectory
            // 
            this.DestinationDirectory.Text = "Destination Directory";
            this.DestinationDirectory.Width = 152;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1229, 584);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ListView sourceListView;
        private System.Windows.Forms.ListView destinationListView;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ColumnHeader sFileName;
        private System.Windows.Forms.ColumnHeader sModified;
        private System.Windows.Forms.ColumnHeader dName;
        private System.Windows.Forms.ColumnHeader dModified;
        private System.Windows.Forms.Button connectButton;
        private System.Windows.Forms.ComboBox sAndDDropdown;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button RightToLeftButton;
        private System.Windows.Forms.Button LeftToRightButton;
        private System.Windows.Forms.ListView moveListView;
        private System.Windows.Forms.ColumnHeader fName;
        private System.Windows.Forms.ColumnHeader moved;
        private System.Windows.Forms.Label destinationLabel;
        private System.Windows.Forms.Label sourceLabel;
        private System.Windows.Forms.Button folderUpButton;
        private System.Windows.Forms.ColumnHeader DestinationDirectory;
    }
}

